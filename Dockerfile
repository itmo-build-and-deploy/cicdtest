FROM ubuntu:latest

RUN apt update && apt install -y build-essential libcurl4-openssl-dev
COPY src/ /code/
WORKDIR /code
RUN make